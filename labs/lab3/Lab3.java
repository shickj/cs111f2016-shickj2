
//***************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Jacob Shick
//Lab 3
//09 14 2016
//
//Purpose:
//***************************
import java.util.Date; // needed for printing todays date
import java.util.Scanner;
public class Lab3 
{
	//------------------------
	// main method: program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		//Label outpus with name and date:
		System.out.println("Jacob Shick\nLab 3\n" + new Date() + "\n");
		
Scanner scan = new Scanner(System.in);

		//Variable dictionary:
		
		float bill;
		float pertip;
		float tip;
		float total;
		float people; 
		float splittotal;
		boolean yn;		//using this for a yes or no question 
		boolean yn2;	

		//output
		System.out.println("Hello, Please enter your name: ");          
		String name = scan.nextLine();    				//scanning for a name
		System.out.println("\nThank you," + name);			// spitting the name back out
		System.out.println("Please enter your bill amount: ");
		bill = scan.nextFloat();					// 
		System.out.println("You entered: " + bill);
		System.out.println("Is this correct? (please answer \"true\" or \"false\")");
		yn = scan.nextBoolean(); //checking if entered value is correct the way most ATM's do. then providing two paths of code, one for true, one for false
			if (yn == true)
		{
			System.out.println("Please enter a tip percentage from 0-100:");
			pertip = scan.nextFloat();
			System.out.println("You have entered " + pertip + " %");
			tip = (pertip/100) * bill;
			System.out.println("\nYour tip comes out to be  $" + tip);
 			total = bill + tip;
			System.out.println("\nAnd you total comes out to  $" + total);
			System.out.println("\nWould you like to split this bill? \n(please enter true for yes or false for no)");
		yn2 = scan.nextBoolean(); //asking if they want to split the bill and giving them a true/false responce choice
			if (yn2 == true)
		{
			System.out.println("How many are splitting the check?");
			people = scan.nextFloat();
			splittotal = total / people;
			System.out.println(" Between " + people + " people, each will pay $" + splittotal);
		} 
			if (yn2 == false)
		{
			System.out.println("Ok");
		}
		System.out.println("\n We hope you enjoyed dining with us today!");
		}
			if (yn == false)
		{
			System.out.println("Maybe you should have entered it right the first time........");
		}	
	}
}
