import java.awt.Color;
import java.io.File;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class MandelbrotColor {
    public static void main(String[] args) throws Exception {
        int width = 1920, height = 1080, max = 1000;
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int black = 0; 
        int[] colors = new int[max];
        for (int i = 0; i<max; i++) {//This for loop generates the colors for the next iteration 
            colors[i] = Color.HSBtoRGB(i/7f, 1, i/(i+1f)); //altering these values will change the color
        }

        for (int row = 0; row < height; row++) { //these for loops construct the mandelbrot 
            for (int col = 0; col < width; col++) {
                double c_re = (col - width/2)*4.0/width;
                double c_im = (row - height/2)*4.0/width;
                double x = 0, y = 0;
                double r2;
                int iteration = 0;
                while (x*x+y*y < 4 && iteration < max) {
                    double x_new = x*x-y*y+c_re;
                    y = 2*x*y+c_im;
                    x = x_new;
                    iteration++;
                }
                if (iteration < max) image.setRGB(col, row, colors[iteration]);//sets the color of the specific iteration.
                else image.setRGB(col, row, black);//if the color cant be set because of bound issues, it just sets to black
            }
        }

        ImageIO.write(image, "png", new File("mandelbrot-color--7-1.png"));//creates a png file of the constructed mandelbrot
    }
}
