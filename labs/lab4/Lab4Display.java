//==========================================
////Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Jacob Shick
//Lab 4
//09 21 2016
//
//Purpose: to create a graphical display that is a masterpiece of computing and artistic ability
//==========================================

import javax.swing.*;

public class Lab4Display
{
  public static void main(String[] args)
  {
    JFrame window = new JFrame(" Jacob Shick ");

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new Lab4());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(600, 400);

  }
}

