//=================================================
//
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page)
  {
	final int WIDTH = 600;
	final int HEIGHT = 400;
	
	//background
	page.setColor(Color.cyan);
	page.fillRect(0,0,WIDTH, WIDTH);
	// sand 
	page.setColor(Color.yellow);
	page.fillRect(0,HEIGHT - 115,WIDTH,HEIGHT);
	//body 
	page.setColor(Color.lightGray);
	page.fillRect(270,150,110,155);
	//arms, legs and features
	page.setColor(Color.black);
	page.drawArc(290,135,180,100,270,70); //right arm
	page.drawLine(270,230,200,200); //left arm
	page.drawOval(279,170,32,32);	//left eye
	page.drawOval(330,165,37,37);	//right eye
	page.fillOval(289,180,15,15);	//left eye middle
	page.fillOval(335,170,15,15);	//right eye middle
	page.drawOval(315,197,20,20);  //nose
	page.fillOval(284,218,80,45); //mouth
	page.drawLine(270,266,378,266); //top of shirt
	page.drawLine(309,266,324,277); //part of tie
	page.drawLine(339,266,324,277); //tie
	page.drawLine(318,272,310,288); //more tie
	page.drawLine(330,272,338,288); //tie piece
	page.drawLine(290,305,290,345); //tie
	page.drawLine(345,305,345,345); //tie
	page.drawLine(290,345,275,345);  //left leg
	page.drawLine(345,345,333,345);  //right leg
	page.drawLine(310,288,324,300);  //left foot
	page.drawLine(338,288,324,300);  //right foot 

  }
}
