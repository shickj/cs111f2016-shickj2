
//***************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Jacob Shick
//Lab 2
//09 07 2016
//
//Purpose: to compute and pring the number of yards between the earth and the moon.
//***************************
import java.util.Date; // needed for printing todays date

public class Lab2 
{
	//------------------------
	// main method: program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		//Label outpus with name and date:
		System.out.println("Jacob Shick\nLab #\n" + new Date() + "\n");

		//Variable dictionary:
		int esbheight = 102; //stories in the empire state building
		int ftperstory = 11;   // average height of a story in feet
		int inperft = 12;  // inches in a foot
		int skperin = 3; //how many skittles per inch
		int esbheightin;		 // empire state building height in skittles
		int heightft;			// height in feet
		int heightin;			// height in inches 
		// compute values
		esbheightin = esbheight * ftperstory * inperft * skperin;
		heightft = esbheight * ftperstory;
		heightin = esbheight * ftperstory * inperft;

		System.out.println("Ever wonder how tall the Empire State Builing is? ");
		System.out.println("maybe in feet?  " + heightft + "  feet");
		System.out.println("maybe in inches? " + heightin + "  inches");
		System.out.println("but how about in number of skittles?!");
		System.out.println(" ");
		System.out.println("In fact the Emprire State Building is approximately..");
		System.out.println(+ esbheightin + "   Skittles");
		System.out.println("thats alot of Skittles!");

	}
}
