//***************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Jacob Shick
//Lab 8
//10 26 2016
//
//Purpose:
//***************************
public class SudokuChecker1 {

	
		//Variable dictionary:
		String row1, row2, row3, row4; //needed for scanning in the board
		String r1c1, r1c2, r1c3, r1c4, r2c1, r2c2, r2c3, r2c4, r3c1, r3c2, r3c3, r3c4, r4c1, r4c2, r4c3, r4c4; // all the vaiables for the grid in string form
		int r1, r2, r3, r4; // all the vars for the sums for the rows
		int c1, c2, c3, c4; // all the variables for the sums of collumns
		int s1, s2, s3, s4; // the vars for the sum of squares

	
		//breaking up the input into the short strings
	public String inputparse (
			String row1,
			String row2,
			String row3,
			String row4) {		

	r1c1 = row1.substring(0,1);
	r1c2 = row1.substring(1,2);
	r1c3 = row1.substring(2,3);
	r1c4 = row1.substring(3,4);

	r2c1 = row2.substring(0,1);
	r2c2 = row2.substring(1,2);
	r2c3 = row2.substring(2,3);
	r2c4 = row2.substring(3,4);

	r3c1 = row3.substring(0,1);
	r3c2 = row3.substring(1,2);
	r3c3 = row3.substring(2,3);
	r3c4 = row3.substring(3,4);

	r4c1 = row4.substring(0,1);
	r4c2 = row4.substring(1,2);
	r4c3 = row4.substring(2,3);
	r4c4 = row4.substring(3,4);

	//converting the short strings to ints for calculations

	int R1c1 = Integer.parseInt(r1c1);
	int R1c2 = Integer.parseInt(r1c2);
	int R1c3 = Integer.parseInt(r1c3);
	int R1c4 = Integer.parseInt(r1c4);

	int R2c1 = Integer.parseInt(r2c1);
	int R2c2 = Integer.parseInt(r2c2);
	int R2c3 = Integer.parseInt(r2c3);
	int R2c4 = Integer.parseInt(r2c4);

	int R3c1 = Integer.parseInt(r3c1);
	int R3c2 = Integer.parseInt(r3c2);
	int R3c3 = Integer.parseInt(r3c3);
	int R3c4 = Integer.parseInt(r3c4);

	int R4c1 = Integer.parseInt(r4c1);
	int R4c2 = Integer.parseInt(r4c2);
	int R4c3 = Integer.parseInt(r4c3);
	int R4c4 = Integer.parseInt(r4c4);

	//calculations 
	
		//rows
		r1 = R1c1 + R1c2 + R1c3 + R1c4;
		r2 = R2c1 + R2c2 + R2c3 + R2c4;
		r3 = R3c1 + R3c2 + R3c3 + R3c4;
		r4 = R4c1 + R4c2 + R4c3 + R4c4;

		//columns
		c1 = R1c1 + R2c1 + R3c1 + R4c1;
		c2 = R1c2 + R2c2 + R3c2 + R4c2;
		c3 = R1c3 + R2c3 + R3c3 + R4c3;
		c4 = R1c4 + R2c4 + R3c4 + R4c4;
		
		//Squares 
		s1 = R1c1 + R2c1 + R1c2 + R2c2;
		s2 = R1c3 + R2c3 + R1c4 + R2c4;
		s3 = R3c1 + R4c1 + R3c2 + R4c2;
		s4 = R3c3 + R4c3 + R3c4 + R4c4;

		return 	
	int r1;		
	int r2;	
	int r3;	
	int r4;	
	int c1;	
	int c2;	
	int c3;	
	int c4;	
	int s1;	
	int s2;	
	int s3;	
	int s4;
	}
public int inputcheck (
	int r1,		
	int r2,	
	int r3,	
	int r4,	
	int c1,	
	int c2,	
	int c3,	
	int c4,	
	int s1,	
	int s2,	
	int s3,	
	int s4) {	

	//checking the  grid
	if (r1 == 10) {
	System.out.println("Row 1: Good");
	
	if (r2 == 10) {
	System.out.println("Row 2: Good");
	
	if (r3 == 10) {
	System.out.println("Row 3: Good");
	
	if (r4 == 10) {
	System.out.println("Row 4: Good");
	
	if (c1 == 10) {
	System.out.println("Column 1: Good");
	
	if (c2 == 10) {
	System.out.println("Column 2: Good");
	
	if (c3 == 10) {
	System.out.println("Column 3: Good");
	
	if (c4 == 10) {
	System.out.println("Column 4: Good");
	
	if (s1 == 10) {
	System.out.println("Square 1: Good");
	
	if (s2 == 10) {
	System.out.println("Square 2: Good");
	
	if (s3 == 10) {
	System.out.println("Square 3: Good");
	
	if (s4 == 10) {
	System.out.println("Square 4: Good");
	System.out.println("\nGreat Job your Sudoku is Correct!");
	}
	else {
	System.out.println("Square 4: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}	else {
	System.out.println("Square 3: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}	else {
	System.out.println("Square 2: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}	else {
	System.out.println("Square 1: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}	else {
	System.out.println("Colomn 4: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}	else {
	System.out.println("Colomn 3: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}	else {
	System.out.println("Colomn 2: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}	else {
	System.out.println("Colomn 1: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}	else {
	System.out.println("Row 4: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}	else {
	System.out.println("Row 3: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}
	else {
	System.out.println("Row 2: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

}	else {
	System.out.println("Row 1: Wrong");
	System.out.println("Your Sudoku is incorrect");	}

	} 
}
}
