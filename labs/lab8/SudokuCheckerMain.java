//***************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Jacob Shick
//Lab #
//09 07 2016
//
//Purpose:
//***************************
import java.util.Date; // needed for printing todays date
import java.util.Scanner;
public class SudokuCheckerMain 
{
	//------------------------
	// main method: program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		SudokuChecker1 checker = new SudokuChecker1();
		
		//Label outpus with name and date:
		System.out.println("Jacob Shick\nLab #\n" + new Date() + "\n");
	
		
		// scanning the input 
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome to the Sukoku Checker! \nPlease begin by entering your solved sudoku, one row at a time with no spaces:");
		System.out.println("Row 1: "); 
		String	row1 = scan.nextLine();
		System.out.println("Row 2: "); 
		String	row2 = scan.nextLine();
		System.out.println("Row 3: "); 
		String	row3 = scan.nextLine();
		System.out.println("Row 4: "); 
		String	row4 = scan.nextLine();

		checker.inputparse(row1, row2, row3, row4);		
	int r1;	
	int r2;	
	int r3;	
	int r4;	
	int c1;	
	int c2;	
	int c3;	
	int c4;	
	int s1;	
	int s2;	
	int s3;	
	int s4;
		checker.inputcheck ( r1,		
	 r2,	
	 r3,	
	 r4,	
	 c1,	
	 c2,	
	 c3,	
	 c4,	
	 s1,	
	 s2,	
	 s3,	
	 s4);
	}
}
