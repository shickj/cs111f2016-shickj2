//***************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Jacob Shick
//Practical 9
//11 18 2016
//
//Purpose: Learning to use exception handleing to make our programs robust
//***************************
public class ExceptionExample {

  static void throwsExceptions(int k, int[] A, String S) {
    int j = 1 / k;
    int len = A.length + 1;
    char c;
	Object cheese = new Integer(6);

    try {
      System.out.println((String)cheese);
      c = S.charAt(0);
      if (k == 10) j = A[3];
    }
    catch (ArrayIndexOutOfBoundsException ex) {
      System.out.println("Array error");
    }
    catch (ArithmeticException ex) {
      System.out.println("Arithmetic error");
    }
    catch (NullPointerException ex) {
      System.out.println("Null pointer");
    }
    catch (ClassCastException ex) {
	System.out.println("Na Na Na you can't do that");
}
    finally {
      System.out.println("In the finally clause");
    }
    System.out.println("After the try block");
  }

  public static void main(String[] args) {
    int[] X = {0,1,2};
    // throwsExceptions(0, X, "hi");
    // throwsExceptions(10, X, "");
    // throwsExceptions(10, X, "bye");
    throwsExceptions(10, X, null);
  }

}
