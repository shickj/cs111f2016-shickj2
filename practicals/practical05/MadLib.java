//***************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Jacob Shick
//Practical 5
//10 14 2016
//
//Purpose: to create a mad lib the takes user input for words and adjectives
//***************************
import java.util.Date; // needed for printing todays date
import java.util.Scanner;
public class MadLib 
{
	//------------------------
	// main method: program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		//Label outpus with name and date:
		System.out.println("Jacob Shick\nLab #\n" + new Date() + "\n");

	Scanner scan = new Scanner(System.in);
	
	System.out.println("Lets make a Mad Lib! \n ");
	System.out.println("Here is what we need from you: \n");
	System.out.println("Please enter a name:");
	String name1 = scan.nextLine(); 
	System.out.println("Please enter an adjective: ");
	String adj1 = scan.nextLine();
	System.out.println("Please enter a name: ");
	String name2 = scan.nextLine();
	System.out.println("Please enter an integer:");
	int num1 = scan.nextInt();
	scan.nextLine();
	System.out.println("Please enter a noun: ");
	String noun1 = scan.nextLine();
	System.out.println("Please enter a verb in the past tense: ");
	String verb1 = scan.nextLine();
	System.out.println("Please enter a non-zero number:");
	float num2 = scan.nextFloat();
	scan.nextLine();
	System.out.println("Please enter an adjective:");
	String adj2 = scan.nextLine();
	System.out.println("Please enter a bodypart: ");
	String noun2 = scan.nextLine();
	

//the madlib 

	System.out.println("Your Mad Lib: \n");
	System.out.println("One day," + name1 + " and his/her " + adj1 + " friend " + name2 + " went on an adventure.");
	System.out.println("They found themselves " + num1 + " " + noun1 +".");
	System.out.println("They " + verb1 +  " with the " + noun1 + " for " + num2 + " hours!");
	System.out.println("After they " + verb1 + " their " + adj2 +" " + noun2 + " hurt for the rest of the day!");














	}
}
