//***************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Jacob Shick
//Practical 6
//10 24 2016
//
//Purpose:make a program that makes the user guess a number from 1-100
//***************************
import java.util.Date; // needed for printing todays date
import java.util.Scanner;
import java.util.Random;
public class GuessMyNumber 
{
	//------------------------
	// main method: program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		//Label outpus with name and date:
		System.out.println("Jacob Shick\nPractical 6\n" + new Date() + "\n");

		//Variable dictionary:
	int num;
	Random r = new Random();
	num = r.nextInt(99) + 1;
	
	// guessing
	int guess;
	int inputs = 0;
	System.out.println("Whats my number?");
	Scanner scan = new Scanner(System.in); 
	guess = scan.nextInt();
	inputs ++;
	while (guess != num) {

	if ( guess < num) {
	System.out.println("Nope that's too low");
	inputs ++;
	}
	else if (guess > num) { 
	System.out.println("Nope that's too high");
	inputs ++;
	}

	guess = scan.nextInt();
	}

	System.out.println("You got it!");
	}
}
