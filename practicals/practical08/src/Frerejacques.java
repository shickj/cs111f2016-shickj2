import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import org.jfugue.Note;
import org.jfugue.Pattern;
import org.jfugue.PatternTransformer;
import org.jfugue.Player;
import org.jfugue.extras.ReversePatternTransformer;

public class Frerejacques
{
  public static void main(String[] args)
  {	

	int repeat;
	System.out.println("How many times would you like to listen to the masterpiece \"Frere Jacques\"?");
	Scanner fran = new Scanner(System.in); 
	repeat = fran.nextInt();
      for (int a=1; a<=repeat; a++ ){

 // "Frere Jacques"
    String[] pattern1Array = {"C5q", "D5q", "E5q", "C5q"};
    StringBuilder builder = new StringBuilder();
    for(String partialPattern : pattern1Array) {
      builder.append(partialPattern);
      builder.append(" ");
    }
    Pattern pattern1 = new Pattern(builder.toString());

    // "Dormez-vous?"
    Pattern pattern2 = new Pattern("E5q F5q G5h");

    // "Sonnez les matines"
    Pattern pattern3 = new Pattern("G5i A5i G5i F5i E5q C5q");

    // "Ding ding dong"
    Pattern pattern4 = new Pattern("C5q G4q C5h");

    // Put all of the patters together to form the song
 	Pattern song = new Pattern();
    song.add(pattern1, 2); // Adds 'pattern1' to 'song' twice
    song.add(pattern2, 2); // Adds 'pattern2' to 'song' twice
    song.add(pattern3, 2); // Adds 'pattern3' to 'song' twice
    song.add(pattern4, 2); // Adds 'pattern4' to 'song' twice

    // Play the song!  

  Player player = new Player(); player.play(song);
   
    try {
      player.saveMidi(song, new File("frerejacues.mid"));
    } catch (IOException e) {
      e.printStackTrace();
    }

    player.close();
}
  }

}






