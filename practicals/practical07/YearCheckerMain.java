//*****************************
// CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;

public class YearCheckerMain
{
    public static void main ( String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;
	boolean leap;
	boolean cicada;
	boolean solar; 
        System.out.println("Please enter a year between 1000 and 3000!");
        userInput = scan.nextInt();

        YearChecker activities = new YearChecker(userInput);

	leap = activities.isLeapYear(userInput);
		
	if (leap == true){
System.out.println("This is a Leap year!");
	} 
	else 
	System.out.println("This is not a Leap year");

	cicada = activities.isCicadaYear();
	if (cicada == true){
System.out.println("This is a Cicada year!");
	} 
	else 
	System.out.println("This is not a Cicada year");
	
	solar = activities.isSunspotYear();
	if (solar == true){
System.out.println("This is a Sunspot year!");
	} 
	else 
	System.out.println("This is not a Sunspot year");

	
        // TO DO: include method calls
	
        System.out.println("Thank you for using this program.");
    }
}
