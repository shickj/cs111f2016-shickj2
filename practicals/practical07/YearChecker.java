//*****************************
// CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: determine if user's year input is a leap year,
// cicada brood II emergence year, or a peak sunspot year.
//*****************************

import java.util.Scanner;

public class YearChecker
{
    //Create instance variables
    int year;

    //Create a constructor
    public YearChecker(int y)
    {
        year = y;
    }

    // a method that checks if the user's input year is a leap year
    public boolean isLeapYear(int year)
    {
        if ( year % 4 == 0 ) {
		return true; 
	}	
	else 
	return false;
    }

    // a method that checks if the user's input year is a cicada year
    public boolean isCicadaYear()
    {
          if ( year % 17 == 0 ) {
		return true; 
	}	
	else 
	return false;

    }

    // a method that check if the user's input year is a sunspot year
    public boolean isSunspotYear()
    {
 
	if ( year % 11 == 0 ) {
		return true; 
	}	
	else 
	return false;

        // TO DO: complete
    }
}
