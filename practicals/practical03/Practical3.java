//***************************
// Honor code: The work I am submitting is a result of my own thinking and efforts
// Jacob Shick
// CMPSCI 111 Fall 2016
// Practical 3
// Dat: 09 016 2016
// 
// Purpose: to compute and print the number of people in Minnesota who do not live in the Twin Cities ( fix this program)
//***************************
import java.util.Date; // needed for today's date  // Had to fix "until" to "util" 

public class Practical3  // needed to match class with file name
{
    // main method: program execution begins here
       public static void main(String[] args) //spacing was messed up 
	{
	  //Label output with name and date: 
	  System.out.println("Miriam Palmer\nLab 2\n" + new Date() + "\n");

	  // Variable dictionary: 
	  int PopMinnesota = 5489594;// Population in Minnesota
	  int PopMinneapolis = 410939;// Population in Minneapolis
	  int Under18MPLSPercent  = 20;// Percentage of MPLS population under eighteen //had to change 20.2 to 20 for int command
          int PopStPaul = 300851;// Population in St Paul
	  int Under18StPaulPercent = 25;// Percentage of St Paul population under eighteen
	  int NonTwinCitypop;// Population of people in Minnesota who do not live in the Twin Cities


	  // Compute values: 
	  NonTwinCitypop = PopMinnesota - ((PopMinneapolis / Under18MPLSPercent) + (PopStPaul / Under18StPaulPercent)); //typos in this line, didnt recognize them as defined variables
	
	  System.out.println("Population in Minnesota: " + PopMinnesota);
			   //Displays population of Minnesota
	  System.out.println("Population in Minneapolis:" + PopMinneapolis);
			   // Displays population of Minneapolis 
	  System.out.println("Percentage of MPLS population under eighteen:"
 			     + Under18MPLSPercent);
			   // Displays the percent of the population of Minneapolis under 18
	  System.out.println("Population in St Paul:" + PopStPaul);//had to add semicolons a couple places
			   // Displays population St Paul
	  System.out.println("Percentage of St Paul population under 18:" 
			     + Under18StPaulPercent); //had to fix capitalization of variable
			   // Displays the percent of the population of St. Paul under 18
	  System.out.println("Population of people in Minnesota who do not live in the Twin Cities" + NonTwinCitypop);
			   // Displays population of people in Minnesota who don't live in the Twin Cities

	}
}

// commentary
// Fixing the bugs wasnt too hard at this stage with the few commands we know, simple things like missed semicolons, the java compiler will help me find which is nice. things like changing an int to a double or typos in variables are a little harder to locate but eventually I was able to find them all. My partner didnt really throw any curveballs at me so it was relatively straight forward. 
